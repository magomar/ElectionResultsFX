package electionresults.io;

import electionresults.persistence.io.PollResultsFileDriver;
import electionresults.persistence.jaxb.*;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 25/04/2017.
 */
public class CSVImporter {

    private static ElectionResultsType importCSV(String resultsFile, String partiesFile, int year) {
        ElectionResultsType pollResults = new ElectionResultsType();
        pollResults.setYear(year);
        InputStream inputStream = ClassLoader.getSystemResourceAsStream(partiesFile);
        Scanner scanner = new Scanner(inputStream);
        scanner.useDelimiter(";|\r?\n|\r");
        scanner.nextLine();
        scanner.nextLine();
        while (scanner.hasNext()) {
            PartyType party = new PartyType();
            party.setAcronym(scanner.next());
            party.setName(scanner.next());
            pollResults.getParty().add(party);
        }

        inputStream = ClassLoader.getSystemResourceAsStream(resultsFile);
        scanner = new Scanner(inputStream);
        scanner.useDelimiter(";|\r?\n|\r");
        scanner.nextLine();
        for (int i = 0; i < 9; i++) {
            scanner.next();
        }
        String[] partyAcronym = new String[pollResults.getParty().size()];
        for (int i = 0; i < partyAcronym.length; i++) {
            partyAcronym[i] = scanner.next();
        }
        while (scanner.hasNext()) {
            RegionResultsType regionResults = new RegionResultsType();
            regionResults.setRegion(scanner.next());
            String province = scanner.next();
            PollDataType pollData = new PollDataType();
            pollData.setVotes(scanner.nextInt());
            pollData.setAbstentions(scanner.nextInt());
            pollData.setNullVotes(scanner.nextInt());
            pollData.setValidVotes(scanner.nextInt());
            pollData.setBlankVotes(scanner.nextInt());
            pollData.setCandidateVotes(scanner.nextInt());
            pollData.setCensus(scanner.nextInt());
            regionResults.setRegionData(pollData);
            for (int i = 0; i < partyAcronym.length; i++) {
                PartyResultsType partyResults = new PartyResultsType();
                partyResults.setAcronym(partyAcronym[i]);
                try {
                    partyResults.setVotes(scanner.nextInt());
                } catch (InputMismatchException ex) {
                    partyResults.setVotes(0);
                }
                regionResults.getPartyResults().add(partyResults);
            }

            if (!province.equals("")) {
                pollResults.getRegionResults().add(regionResults);
                RegionType region = new RegionType();
                region.setName(regionResults.getRegion());
                region.setProvince(province);
                pollResults.getRegion().add(region);
            }
            else {
                pollResults.setGlobalResults(regionResults);
            }
        }

        return pollResults;
    }


    private static String defaultResultsFile(int year) {
        return "data/Resultados" + year + ".csv";
    }

    private static String defaultPartiesFile(int year) {
        return "data/Candidaturas" + year + ".csv";
    }

    public static void main(String[] args) {
        int[] electionYears = {1995, 1999, 2003, 2007, 2011, 2015};
        PollResultsFileDriver fileDriver = new PollResultsFileDriver();
        Path path = null;
        File file = null;
        for (int year : electionYears) {
            ElectionResultsType pollResults = importCSV(defaultResultsFile(year), defaultPartiesFile(year), year);
            try {
                path = Paths.get(System.getProperty("user.home") + File.separator + year + "elections.xml");
                file = Files.createFile(path).toFile();
            } catch (FileAlreadyExistsException e) {
                System.err.println("File already exists: " + path);
                file = path.toFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            fileDriver.save(pollResults, file);

        }


    }

}
