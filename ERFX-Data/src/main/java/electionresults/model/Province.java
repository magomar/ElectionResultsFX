package electionresults.model;

import java.util.HashMap;
import java.util.Map;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 27/04/2017.
 */
public enum Province  {
    ALICANTE("Alicante", 35),
    CASTELLON("Castellón", 24),
    VALENCIA("Valencia", 40);

    private final String fullname;
    private final int seats;
    private final String imagePath;
    public static final Map<String, Integer> PROVINCE_SEATS = new HashMap<>();
    public static final Map<String, Province> PROVINCES_BY_NAME = new HashMap<>();

    Province(String fullname, int seats) {
        this.fullname = fullname;
        this.seats = seats;
        imagePath = "/images/maps/map_" + name().toLowerCase() + ".png";
    }

    public String getFullname() {
        return fullname;
    }

    public int getSeats() {
        return seats;
    }

    static {
        for (Province province : values()) {
            PROVINCES_BY_NAME.put(province.fullname, province);
            PROVINCE_SEATS.put(province.fullname, province.seats);
        }
    }

    public static Province getProvinceByName(String name) {
        return PROVINCES_BY_NAME.get(name);
    }

    public static Integer getSeatsByProvince(String name) {
        return PROVINCE_SEATS.get(name);
    }

    public String getImagePath() {
        return imagePath;
    }

    public String toString() {
        return fullname;
    }

    public static int getTotalSeats() {
        int seats = 0;
        for (Integer provinceSeats : PROVINCE_SEATS.values()) {
            seats += provinceSeats;
        }
        return seats;
    }
}
