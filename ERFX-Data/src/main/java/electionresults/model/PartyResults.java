package electionresults.model;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 28/04/2017.
 */
public class PartyResults implements Comparable<PartyResults> {
    protected final String party;
    protected final int votes;
    protected final double percentage;
    private int seats;

    public PartyResults(String party, int votes, int validVotes) {
        this.party = party;
        this.votes = votes;
        percentage = 100.0 * (double) votes / validVotes;
    }

    /**
     * Returns the name of the party used in this election
     * @return the name of the party
     */
    public String getParty() {
        return party;
    }

    /**
     * Returns the number of votes achieved in this election
     * @return the number of votes
     */
    public int getVotes() {
        return votes;
    }

    /**
     * Returns the proportion of votes received by this party, compared with the total number of valid votes (not null
     * votes)
     * @return the percentage of votes
     */
    public double getPercentage() {
        return percentage;
    }

    /**
     * Increases the number of seats assigned to this party in a certain region
     * @param seats the number of seats assigned
     */
    public void addSeats(int seats) {
        this.seats += seats;
    }

    /**
     * Returns the number of seats obtained by this party
     * @return the number of seats assigned
     */
    public int getSeats() {
        return seats;
    }


    @Override
    public int compareTo(PartyResults o) {
        int seatsComparison = Integer.compare(this.seats, o.seats);
        if (seatsComparison != 0) return seatsComparison;
        else return Integer.compare(this.votes, o.votes);
    }

}
