package electionresults.model;

import electionresults.persistence.jaxb.PartyResultsType;
import electionresults.persistence.jaxb.PollDataType;

/**
 * Class used to represent basic statistical data about an election poll, like the census, the number of valid votes, etc.
 * This class can be used indistinctly to represent global data or partial data (for a given region, for instance)
 *
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 26/04/2017.
 */

public class PollData {
    private int votes;
    private int abstentions;
    private int nullVotes;
    private int validVotes;
    private int blankVotes;
    private int candidateVotes;
    private int census;

    public PollData(int votes, int abstentions, int nullVotes, int validVotes, int blankVotes, int candidateVotes, int census) {
        this.votes = votes;
        this.abstentions = abstentions;
        this.nullVotes = nullVotes;
        this.validVotes = validVotes;
        this.blankVotes = blankVotes;
        this.candidateVotes = candidateVotes;
        this.census = census;
    }

    /**
     * Returns the total number of votes registered, including bot valid and null votes
     * @return the amount of votes emitted
     */
    public int getVotes() {
        return votes;
    }

    /**
     * Returns the number of abstentions, that is, people in the census that didn't vote
     * @return the amount of abstentions
     */
    public int getAbstentions() {
        return abstentions;
    }

    /**
     * Returns the number of null votes, that is, votes that are not valid
     * @return the amount of null votes
     */
    public int getNullVotes() {
        return nullVotes;
    }

    /**
     * Returns the number of valid votes, which includes also the blank votes
     * @return the amount of valid votes
     */
    public int getValidVotes() {
        return validVotes;
    }

    /**
     * Returns the number of blank votes, that is, votes without a candidate
     * @return the amount of blank votes
     */
    public int getBlankVotes() {
        return blankVotes;
    }

    /**
     * Gets the number of votes with a valid candidate, that is, votes which are valid and non-blank
     * @return the amount of candidate votes
     */
    public int getCandidateVotes() {
        return candidateVotes;
    }

    /**
     * Returns the amount of people that is allowed to vote in this election
     * @return the census
     */
    public int getCensus() {
        return census;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Censo electoral: ").append(census).append('\n');
        sb.append("Votos emitidos: ").append(votes).append('\n');
        sb.append("Abstenciones: ").append(abstentions).append('\n');
        sb.append("Votos nulos: ").append(nullVotes).append('\n');
        sb.append("Votos validos: ").append(validVotes).append('\n');
        sb.append("Votos en blanco: ").append(blankVotes).append('\n');
        sb.append("Votos con candidato: ").append(candidateVotes).append('\n');
        sb.append("Reparto de votos:\n");
        return sb.toString();
    }

//    Censo = Abstenciones + Nulos + Validos
//    Candidaturas = Validos - Blancos
//    Votantes = Validos + Nulos

}
