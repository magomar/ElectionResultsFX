package electionresults.model;

import java.util.HashMap;
import java.util.Map;

/**
 * This class holds all the information corresponding to a single election.
 *
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 25/04/2017.
 */
public class ElectionResults {
    private final int year;
    private final Map<String, String> regionProvinces = new HashMap<>();
    private final Map<String, ProvinceInfo> provinces = new HashMap<>();
    private final Map<String, String> partyNames = new HashMap<>();
    private final Map<String, RegionResults> regionResults = new HashMap<>();
    private final Map<String, RegionResults> provinceResults = new HashMap<>();
    private RegionResults globalResults;

    public ElectionResults(int year) {
        this.year = year;
    }

    /**
     * Returns the year the election took place
     *
     * @return the year
     */
    public int getYear() {
        return year;
    }

    /**
     * Returns the names of the provinces corresponding to every region involved in the election
     *
     * @return the mapping of regions to provinces
     */
    public Map<String, String> getRegionProvinces() {
        return regionProvinces;
    }

    /**
     * Returns information about every province, respresented as a map that pairs the name of the province to their info
     *
     * @return the mapping of province names to their infos
     * @see ProvinceInfo
     */
    public Map<String, ProvinceInfo> getProvinces() {
        return provinces;
    }

    /**
     * Returns the  names of the parties participating in the election
     *
     * @return the mapping of party acronyms to party names
     */
    public Map<String, String> getPartyNames() {
        return partyNames;
    }

    /**
     * Returns the global electoral results
     *
     * @return the electoral results
     * @see RegionResults
     */
    public RegionResults getGlobalResults() {
        return globalResults;
    }

    /**
     * Returns the electoral results in every province involved in the election
     *
     * @return the mapping of provinces to electoral results
     * @see RegionResults
     */
    public Map<String, RegionResults> getProvinceResults() {
        return provinceResults;
    }

    /**
     * Returns the electoral results in the province passed as parameter
     *
     * @return the electoral results in the province
     * @param province the province to get results from
     * @see RegionResults
     */
    public RegionResults getProvinceResults(String province) {
        return provinceResults.get(province);
    }

    /**
     * Returns the electoral results in every region involved in the election
     *
     * @return the mapping of regions to electoral results
     * @see RegionResults
     */
    public Map<String, RegionResults> getRegionResults() {
        return regionResults;
    }

    /**
     * Returns the electoral results in the region passed as parameter
     *
     * @return the electoral results in the region
     * @param region the region to get results from
     * @see RegionResults
     */
    public RegionResults getRegionResults(String region) {
        return regionResults.get(region);
    }

    /**
     * Sets the global results, for the entire constituency where the elections took place
     *
     * @param globalResults the aggregated results of the elections
     */
    public void setGlobalResults(RegionResults globalResults) {
        this.globalResults = globalResults;
    }
}
