package electionresults.model;

import java.util.*;

/**
 * Class used to represent all available data about a single region, which includes the basic statistical data
 * ({@link #pollData}  and the number of votes obtained by each party.
 *
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 26/04/2017.
 */
public class RegionResults {
    protected final String region;
    protected final PollData pollData;
    protected final Map<String, PartyResults> partyResults = new HashMap<>();

    public RegionResults(String region, PollData pollData) {
        this.region = region;
        this.pollData = pollData;
    }

    /**
     * Returns the name of the region where this results occurred
     *
     * @return the name of the region
     */
    public String getRegion() {
        return region;
    }

    /**
     * Returns the basic statistical data about the polling in this region
     *
     * @return the polling data
     */
    public PollData getPollData() {
        return pollData;
    }

    /**
     * Returns the electoral results for every party in this region
     *
     * @return the map of parties to their electoral results
     */
    public Map<String, PartyResults> getPartyResults() {
        return partyResults;
    }

    /**
     * Returns the electoral results in this region for the party passed as parameter
     *
     * @param party the party to get results for
     * @return the electoral results of the party
     */
    public PartyResults getPartyResults(String party) {
        return partyResults.get(party);
    }

    /**
     * Returns the electoral results for every party in this region, sorted in descending (reverse) order. This method
     * returns a copy of the original list, therefore, in order to add/remove items, the {@link #getPartyResults()}
     * method should be used instead.
     * Note that the sorting is performed considering first the number of seats assigned, then the number of votes.
     * @return the list of party results sorted
     */
    public List<PartyResults> getPartyResultsSorted() {
        List<PartyResults> partyResultsList = new ArrayList<>(partyResults.values());
        partyResultsList.sort(Comparator.reverseOrder());
        return partyResultsList;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(region).append('\n');
        sb.append(pollData.toString());
        for (PartyResults results : getPartyResultsSorted()) {
            sb.append("   ").append(results.getParty()).append(": ").append(results.getVotes()).append('\n');
        }
        return sb.toString();
    }
}
