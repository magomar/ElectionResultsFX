package electionresults.model;

import java.util.List;

/**
 * Class used to hold useful information about a province, including the list of regions it is composed of and the
 * number of seats available in this province. Note that regions in general do not have seats available; only provinces
 * are constituencies (electoral districts) in the elections for an Autonomous Community.
 *
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 08/05/2017.
 */
public class ProvinceInfo {
    private final String province;
    private final List<String> regions;
    private final int seats;

    public ProvinceInfo(String province, List<String> regions, int seats) {
        this.province = province;
        this.regions = regions;
        this.seats = seats;
    }

    /**
     * Returns the name of the province
     * @return the name of the province
     */
    public String getProvince() {
        return province;
    }

    /**
     * Returns the list of regions that are part of this province
     * @return the list of regions
     */
    public List<String> getRegions() {
        return regions;
    }

    /**
     * Returns the number of seats available for election in this province
     * @return the number of seats
     */
    public int getSeats() {
        return seats;
    }
}
