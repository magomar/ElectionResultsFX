package electionresults.model;

import javafx.scene.image.Image;
import javafx.scene.paint.Color;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 29/04/2017.
 */
public enum Party {
    PP(Color.rgb(0,163,223), "PP.png", "PP"),
    PSOE(Color.rgb(226,36,25), "PSOE.png", "PSOE", "PSOE-P"),
    PODEMOS(Color.rgb(104,48,100), "Podemos.png", "PODEMOS"),
    COMPROMIS(Color.rgb(200,90,0), "Compromis.png", "COMPROMÍS", "COMPROMÍS PV", "COALICIÓ COMPROMÍS"),
    CS(Color.rgb(235,85,15), "Cs.png", "C's"),
    UV(Color.rgb(22,94,152), "UnioValenciana.png", "UV","UV-FICVA-CCV", "UNIO-UNIÓN"),
    UPYD(Color.rgb(236,0,140), "UPyD.png","UPyD"),
    EU(Color.rgb(220,11,40), "EU.png","EU", "EU-EV", "EUPV", "BLOC-EV", "EUPV-EV-ERPV-AS:AC", "ENTESA");

    public static final Map<String, Party> PARTIES_BY_NAME = new HashMap<>();
    private List<String> acronyms;
    private Color color;
    private String hexColor;
    private Image logo;
//    private URL info;

    Party(Color color, String logo, String... acronyms) {
        this.acronyms = Arrays.asList(acronyms);
        this.color = color;
        this.hexColor = toRGBCode(color);
        this.logo = new Image(getClass().getResourceAsStream("/images/parties/" + logo));
        //TODO add wikipedia url for each party
    }

    static {
        for (Party party : values()) {
            for (String acronym : party.getAcronyms()) {
                PARTIES_BY_NAME.put(acronym, party);
            }

        }
    }

    public static Party getPartyByName(String name) {
        return PARTIES_BY_NAME.get(name);
    }

    /**
     * Returns the most common name or acronym of the party
     * @return the name
     */
    public String getName() {
        return acronyms.get(0);
    }

    /**
     * Returns a list of different acronyms used by the same party along different elections.
     * @return the list of acronyms
     */
    public List<String> getAcronyms() {
        return acronyms;
    }

    /**
     * Returns the color used by or associated to the party
     * @return
     */
    public Color getColor() {
        return color;
    }

    /**
     * Returns an image with the logo of the party
     * @return
     */
    public Image getLogo() {
        return logo;
    }

    /**
     * Returns the color used by or associated to the party in hexadecimal format (#rrggbb)
     * @return the color in hexadecimal
     */
    public String getHexColor() {
        return hexColor;
    }

    private static String toRGBCode(Color color )
    {
        return String.format( "#%02X%02X%02X",
                (int)( color.getRed() * 255 ),
                (int)( color.getGreen() * 255 ),
                (int)( color.getBlue() * 255 ) );
    }

}
