package electionresults.persistence.io;

import electionresults.model.ElectionResults;
import electionresults.model.Province;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

/**
 * This class provides a single entry point to access the data for this application.
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 27/04/2017.
 */
public class DataAccessLayer {
    private static final PollResultsFileDriver DRIVER = new PollResultsFileDriver();
    private static long shortdelay = 0;
    private static long longdelay = 0;
    private static List<Integer> electionYears;

    static {
        Properties properties = new Properties();
        try {
            properties.load(DataAccessLayer.class.getResourceAsStream("/config.properties"));
            shortdelay = Long.parseLong(properties.getProperty("dataaccesslayer.shortdelay"));
            longdelay = Long.parseLong(properties.getProperty("dataaccesslayer.longdelay"));
            String[] yearStrings = properties.getProperty("dataaccesslayer.election_years").split(",");
            electionYears = new ArrayList<>();
            for (int i = 0; i < yearStrings.length; i++) {
                electionYears.add(Integer.parseInt(yearStrings[i]));
            }
            electionYears.sort(Comparator.naturalOrder());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns the complete electoral results for a given year
     * @param year the year where the elections took place
     * @return the electoral results
     */
    public static ElectionResults getElectionResults(int year) {
        if (!electionYears.contains(year)) {
            return null;
        }
        String filePath = "/data/" + year + "elections.xml";
        InputStream inputStream = DataAccessLayer.class.getResourceAsStream(filePath);
        ElectionResults electionResults = DRIVER.load(inputStream, Province.PROVINCE_SEATS);
        try {
            TimeUnit.SECONDS.sleep(shortdelay);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return electionResults;
    }

    /**
     * Returns the electoral results for all the available years, sorted by antiquity (older elections first)
     * @return the list of electoral results
     */
    public static List<ElectionResults> getAllElectionResults() {
        List<ElectionResults> results = new ArrayList<>();
        for (int year : electionYears) {
            String filePath = "/data/" + year + "elections.xml";
            InputStream inputStream = DataAccessLayer.class.getResourceAsStream(filePath);
            results.add(DRIVER.load(inputStream, Province.PROVINCE_SEATS));
        }
        try {
            TimeUnit.SECONDS.sleep(longdelay);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return results;
    }

    public static List<Integer> getElectionYears() {
        return electionYears;
    }
}
