package electionresults.persistence.io;

import javax.xml.bind.*;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;


class JAXBTools<T> {
    private static final Logger LOGGER = Logger.getLogger(JAXBTools.class.getName());
    private final Marshaller marshaller;
    private final Unmarshaller unmarshaller;
    private final JAXBContext jaxbContext;
    private final Class<T> rootClass;

    public JAXBTools(Class<T> rootClass) throws JAXBException {
        jaxbContext = JAXBContext.newInstance(rootClass.getPackage().getName());
        unmarshaller = jaxbContext.createUnmarshaller();
        marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        this.rootClass = rootClass;
    }


    /**
     * Unmarshalls XML element from file into java object
     *
     * @param file the XML file to be unmarshalled
     * @return the object unmarshalled from the {@code file}
     */
//    public T unmarshallFile(File file) {
//        T object = null;
//        try {
//            object = unmarshallStream(new FileInputStream(file));
//        } catch (FileNotFoundException ex) {
//            LOGGER.log(Level.SEVERE, "File {} not found: ", file.getName());
//            LOGGER.severe(ex.getMessage());
//        }
//        return object;
//    }

    /**
     * Unmarshalls XML element from InputStream into java object
     *
     * @param inputStream the input stream to be unmarshalled
     * @return the object unmarshalled from the {@code inputStream}
     */
    public T unmarshallStream(InputStream inputStream) {
        T object = null;
        try {
//            JAXBElement<T> root = unmarshaller.unmarshal(new StreamSource(inputStream), rootClass);
            JAXBElement<T> root = (JAXBElement<T>) unmarshaller.unmarshal(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
            object = root.getValue();
        } catch (JAXBException ex) {
            LOGGER.log(Level.SEVERE,"JAXB exception unmarshalling from input stream");
            LOGGER.severe(ex.getMessage());
        }
        return object;
    }

    /**
     * Marshalls Java object into XML file
     *
     * @param object object to be marshalled
     * @param file   file to save the marshalled object
     * @return the XML file
     */
    public File marshallXML(T object, File file) {
        try (FileOutputStream fos = new FileOutputStream(file)) {
            marshaller.marshal(object, fos);
            return file;
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE,"IO exception marshalling to {}", file);
            LOGGER.severe(ex.getMessage());
        } catch (JAXBException ex) {
            LOGGER.log(Level.SEVERE,"JAXB exception marshalling to {}", file);
            LOGGER.severe(ex.getMessage());
        }
        return null;
    }

    /**
     * Marshalls Java object into XML file
     *
     * @param jaxbElement object to be marshalled
     * @param file   file to save the marshalled object
     * @return the XML file
     */
    public File marshallXML(JAXBElement<T> jaxbElement, File file) {
        try (FileOutputStream fos = new FileOutputStream(file)) {
            marshaller.marshal(jaxbElement, fos);
            return file;
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE,"IO exception marshalling to {}", file);
            LOGGER.severe(ex.getMessage());
        } catch (JAXBException ex) {
            LOGGER.log(Level.SEVERE,"JAXB exception marshalling to {}", file);
            LOGGER.severe(ex.getMessage());
        }
        return null;
    }

    public StringWriter marshallXML(T object) {
        try {
            try (StringWriter sw = new StringWriter()) {
                marshaller.marshal(object, sw);
                return sw;
            } catch (IOException ex) {
                LOGGER.log(Level.SEVERE,"IO exception marshalling to String Writer");
                LOGGER.severe(ex.getMessage());
            }
        } catch (JAXBException ex) {
            LOGGER.log(Level.SEVERE,"JAXB exception marshalling to String Writer");
            LOGGER.severe(ex.getMessage());
        }
        return null;
    }

}
