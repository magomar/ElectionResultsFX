package electionresults.persistence.io;

import electionresults.model.*;
import electionresults.persistence.jaxb.*;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.InputStream;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 03/04/2017.
 */
public class PollResultsFileDriver {
    private static final Logger LOGGER = Logger.getLogger(PollResultsFileDriver.class.getName());
    private JAXBTools<ElectionResultsType> jaxbTools;
    private ObjectFactory objectFactory = new ObjectFactory();

    public PollResultsFileDriver() {
        try {
            jaxbTools = new JAXBTools<>(ElectionResultsType.class);
        } catch (JAXBException e) {
            LOGGER.log(Level.SEVERE, "Error creating JAXBTools with context {}", ElectionResultsType.class.getName());
        }
    }


    public ElectionResults load(InputStream is, Map<String, Integer> provinceSeats) {
        ElectionResultsType jaxbType = jaxbTools.unmarshallStream(is);
        return (jaxbType != null ? convertFrom(jaxbType, provinceSeats) : null);
    }

    public void save(ElectionResultsType pollResults, File file) {
        JAXBElement<ElectionResultsType> jaxbElement = objectFactory.createElectionResults(pollResults);
        jaxbTools.marshallXML(jaxbElement, file);
    }

    private static ElectionResults convertFrom(ElectionResultsType electionResultsType, Map<String, Integer> provinceSeats) {
        ElectionResults electionResults = new ElectionResults(electionResultsType.getYear());
        Map<String, String> regionProvinces = electionResults.getRegionProvinces();
        Map<String, List<String>> provinceRegions = new HashMap<>();
        Map<String, List<RegionResults>> regionResultsByProvince = new HashMap<>();
        for (RegionType region : electionResultsType.getRegion()) {
            String regionName = region.getName();
            String provinceName = region.getProvince();
            regionProvinces.put(regionName, provinceName);
            if (!provinceRegions.containsKey(provinceName)) {
                provinceRegions.put(provinceName, new ArrayList<>());
                regionResultsByProvince.put(provinceName, new ArrayList<>());
            }
            provinceRegions.get(provinceName).add(regionName);
        }

        for (PartyType party : electionResultsType.getParty()) {
            electionResults.getPartyNames().put(party.getAcronym(), party.getName());
        }
        Map<String, RegionResults> regionResultsMap = electionResults.getRegionResults();
        for (RegionResultsType regionResultsType : electionResultsType.getRegionResults()) {
            RegionResults regionResults = convertFrom(regionResultsType);
            String region = regionResults.getRegion();
            regionResultsMap.put(region, regionResults);
            String province = regionProvinces.get(region);
            regionResultsByProvince.get(province).add(regionResults);
        }
        Map<String, RegionResults> provinceResultsMap = electionResults.getProvinceResults();
        for (Map.Entry<String, List<RegionResults>> entry : regionResultsByProvince.entrySet()) {
            String province = entry.getKey();
            electionResults.getProvinces().put(province, new ProvinceInfo(province, provinceRegions.get(province), provinceSeats.get(province)));
            provinceResultsMap.put(province, obtainProvinceResults(province, entry.getValue(), provinceSeats.get(province)));
        }
        RegionResults globalResults = convertFrom(electionResultsType.getGlobalResults());
        Map<String, PartyResults> globalPartyResultsMap = new HashMap<>();
        for (PartyResults globalPartyResults : globalResults.getPartyResults().values()) {
                globalPartyResultsMap.put(globalPartyResults.getParty(), globalPartyResults);
        }
        for (RegionResults provinceResults : provinceResultsMap.values()) {
            for (PartyResults partyResults : provinceResults.getPartyResults().values()) {
                globalPartyResultsMap.get(partyResults.getParty()).addSeats(partyResults.getSeats());
            }
        }
        electionResults.setGlobalResults(globalResults);
        return electionResults;
    }

    private static RegionResults obtainProvinceResults(String province, List<RegionResults> regionResults, int seats) {
        int votes = 0;
        int abstentions = 0;
        int nullVotes = 0;
        int validVotes = 0;
        int blankVotes = 0;
        int candidateVotes = 0;
        int census = 0;
        List<String> parties = new ArrayList<>(regionResults.get(0).getPartyResults().size());
        Map<String, Integer> partyVotes = new HashMap<>();
        for (PartyResults pr : regionResults.get(0).getPartyResults().values()) {
            parties.add(pr.getParty());
            partyVotes.put(pr.getParty(), 0);
        }

        for (RegionResults regionResult : regionResults) {
            PollData regionData = regionResult.getPollData();
            votes += regionData.getVotes();
            abstentions += regionData.getAbstentions();
            nullVotes += regionData.getNullVotes();
            validVotes += regionData.getValidVotes();
            blankVotes += regionData.getBlankVotes();
            candidateVotes += regionData.getCandidateVotes();
            census += regionData.getCensus();
            for (PartyResults partyResults : regionResult.getPartyResults().values()) {
                partyVotes.put(partyResults.getParty(), partyVotes.get(partyResults.getParty()) + partyResults.getVotes());
            }
        }
        PollData pollData = new PollData(votes, abstentions, nullVotes, validVotes, blankVotes, candidateVotes, census);
        RegionResults provinceResults = new RegionResults(province, pollData);

        List<PartyResults> partiesWithSeats = new ArrayList<>();
        List<PartyResults> partiesWithoutSeats = new ArrayList<>();
        List<DHondtQuotient> quotients = new ArrayList<>();
        for (String party : parties) {
            PartyResults partyResults = new PartyResults(party, partyVotes.get(party), pollData.getValidVotes());
            if (partyResults.getPercentage() < 5) {
                partiesWithoutSeats.add(partyResults);
            } else {
                partiesWithSeats.add(partyResults);
                for (int i = 1; i <= seats; i++) {
                    quotients.add(new DHondtQuotient(partyResults, partyResults.getVotes() / i));
                }
            }
        }
        quotients.sort(Comparator.reverseOrder());
        for (int i = 0; i < seats; i++) {
            DHondtQuotient hondtQuotient = quotients.get(i);
            hondtQuotient.party.addSeats(1);
        }
        partiesWithSeats.sort(Comparator.reverseOrder());
        partiesWithoutSeats.sort(Comparator.reverseOrder());
        for (PartyResults partyResults : partiesWithSeats) {
            provinceResults.getPartyResults().put(partyResults.getParty(), partyResults);
        }
        for (PartyResults partyResults : partiesWithoutSeats) {
            provinceResults.getPartyResults().put(partyResults.getParty(), partyResults);
        }
        return provinceResults;
    }

    private static class DHondtQuotient implements Comparable<DHondtQuotient> {
        private final PartyResults party;
        private final int quotient;

        private DHondtQuotient(PartyResults party, int quotient) {
            this.party = party;
            this.quotient = quotient;
        }

        @Override
        public int compareTo(DHondtQuotient o) {
            return Integer.compare(this.quotient, o.quotient);
        }
    }

    private static RegionResults convertFrom(RegionResultsType regionResults) {
        PollData pollData = convertFrom(regionResults.getRegionData());
        RegionResults results = new RegionResults(regionResults.getRegion(), pollData);
        for (PartyResultsType partyResults : regionResults.getPartyResults()) {
            results.getPartyResults().put(partyResults.getAcronym(), new PartyResults(partyResults.getAcronym(), partyResults.getVotes(), pollData.getValidVotes()));
        }
        return results;
    }

    private static PollData convertFrom(PollDataType pollData) {
        return new PollData(
                pollData.getVotes(),
                pollData.getAbstentions(),
                pollData.getNullVotes(),
                pollData.getValidVotes(),
                pollData.getBlankVotes(),
                pollData.getCandidateVotes(),
                pollData.getCensus()
        );
    }

//    public static ElectionResultsType convertTo(ElectionResults modelType) {
//        throw new UnsupportedOperationException("Operation not implemented yet");
//    }
}
