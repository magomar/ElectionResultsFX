
package electionresults.persistence.jaxb;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para electionResultsType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="electionResultsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="year" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="party" type="{}partyType" maxOccurs="unbounded"/>
 *         &lt;element name="region" type="{}regionType" maxOccurs="unbounded"/>
 *         &lt;element name="regionResults" type="{}regionResultsType" maxOccurs="unbounded"/>
 *         &lt;element name="globalResults" type="{}regionResultsType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "electionResultsType", propOrder = {
    "year",
    "party",
    "region",
    "regionResults",
    "globalResults"
})
public class ElectionResultsType {

    protected int year;
    @XmlElement(required = true)
    protected List<PartyType> party;
    @XmlElement(required = true)
    protected List<RegionType> region;
    @XmlElement(required = true)
    protected List<RegionResultsType> regionResults;
    @XmlElement(required = true)
    protected RegionResultsType globalResults;

    /**
     * Obtiene el valor de la propiedad year.
     * 
     */
    public int getYear() {
        return year;
    }

    /**
     * Define el valor de la propiedad year.
     * 
     */
    public void setYear(int value) {
        this.year = value;
    }

    /**
     * Gets the value of the party property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the party property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PartyType }
     * 
     * 
     */
    public List<PartyType> getParty() {
        if (party == null) {
            party = new ArrayList<PartyType>();
        }
        return this.party;
    }

    /**
     * Gets the value of the region property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the region property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegionType }
     * 
     * 
     */
    public List<RegionType> getRegion() {
        if (region == null) {
            region = new ArrayList<RegionType>();
        }
        return this.region;
    }

    /**
     * Gets the value of the regionResults property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regionResults property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegionResults().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegionResultsType }
     * 
     * 
     */
    public List<RegionResultsType> getRegionResults() {
        if (regionResults == null) {
            regionResults = new ArrayList<RegionResultsType>();
        }
        return this.regionResults;
    }

    /**
     * Obtiene el valor de la propiedad globalResults.
     * 
     * @return
     *     possible object is
     *     {@link RegionResultsType }
     *     
     */
    public RegionResultsType getGlobalResults() {
        return globalResults;
    }

    /**
     * Define el valor de la propiedad globalResults.
     * 
     * @param value
     *     allowed object is
     *     {@link RegionResultsType }
     *     
     */
    public void setGlobalResults(RegionResultsType value) {
        this.globalResults = value;
    }

}
