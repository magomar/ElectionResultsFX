
package electionresults.persistence.jaxb;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the electionresults.persistence.jaxb package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ElectionResults_QNAME = new QName("", "electionResults");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: electionresults.persistence.jaxb
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ElectionResultsType }
     * 
     */
    public ElectionResultsType createElectionResultsType() {
        return new ElectionResultsType();
    }

    /**
     * Create an instance of {@link RegionType }
     * 
     */
    public RegionType createRegionType() {
        return new RegionType();
    }

    /**
     * Create an instance of {@link PartyResultsType }
     * 
     */
    public PartyResultsType createPartyResultsType() {
        return new PartyResultsType();
    }

    /**
     * Create an instance of {@link PollDataType }
     * 
     */
    public PollDataType createPollDataType() {
        return new PollDataType();
    }

    /**
     * Create an instance of {@link RegionResultsType }
     * 
     */
    public RegionResultsType createRegionResultsType() {
        return new RegionResultsType();
    }

    /**
     * Create an instance of {@link PartyType }
     * 
     */
    public PartyType createPartyType() {
        return new PartyType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ElectionResultsType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "electionResults")
    public JAXBElement<ElectionResultsType> createElectionResults(ElectionResultsType value) {
        return new JAXBElement<ElectionResultsType>(_ElectionResults_QNAME, ElectionResultsType.class, null, value);
    }

}
