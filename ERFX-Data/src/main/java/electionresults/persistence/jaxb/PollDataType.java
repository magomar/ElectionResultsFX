
package electionresults.persistence.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para pollDataType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="pollDataType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="votes" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="abstentions" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="nullVotes" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="validVotes" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="blankVotes" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="candidateVotes" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="census" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "pollDataType", propOrder = {
    "votes",
    "abstentions",
    "nullVotes",
    "validVotes",
    "blankVotes",
    "candidateVotes",
    "census"
})
public class PollDataType {

    protected int votes;
    protected int abstentions;
    protected int nullVotes;
    protected int validVotes;
    protected int blankVotes;
    protected int candidateVotes;
    protected int census;

    /**
     * Obtiene el valor de la propiedad votes.
     * 
     */
    public int getVotes() {
        return votes;
    }

    /**
     * Define el valor de la propiedad votes.
     * 
     */
    public void setVotes(int value) {
        this.votes = value;
    }

    /**
     * Obtiene el valor de la propiedad abstentions.
     * 
     */
    public int getAbstentions() {
        return abstentions;
    }

    /**
     * Define el valor de la propiedad abstentions.
     * 
     */
    public void setAbstentions(int value) {
        this.abstentions = value;
    }

    /**
     * Obtiene el valor de la propiedad nullVotes.
     * 
     */
    public int getNullVotes() {
        return nullVotes;
    }

    /**
     * Define el valor de la propiedad nullVotes.
     * 
     */
    public void setNullVotes(int value) {
        this.nullVotes = value;
    }

    /**
     * Obtiene el valor de la propiedad validVotes.
     * 
     */
    public int getValidVotes() {
        return validVotes;
    }

    /**
     * Define el valor de la propiedad validVotes.
     * 
     */
    public void setValidVotes(int value) {
        this.validVotes = value;
    }

    /**
     * Obtiene el valor de la propiedad blankVotes.
     * 
     */
    public int getBlankVotes() {
        return blankVotes;
    }

    /**
     * Define el valor de la propiedad blankVotes.
     * 
     */
    public void setBlankVotes(int value) {
        this.blankVotes = value;
    }

    /**
     * Obtiene el valor de la propiedad candidateVotes.
     * 
     */
    public int getCandidateVotes() {
        return candidateVotes;
    }

    /**
     * Define el valor de la propiedad candidateVotes.
     * 
     */
    public void setCandidateVotes(int value) {
        this.candidateVotes = value;
    }

    /**
     * Obtiene el valor de la propiedad census.
     * 
     */
    public int getCensus() {
        return census;
    }

    /**
     * Define el valor de la propiedad census.
     * 
     */
    public void setCensus(int value) {
        this.census = value;
    }

}
