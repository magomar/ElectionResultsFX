
package electionresults.persistence.jaxb;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para regionResultsType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="regionResultsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="region" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="regionData" type="{}pollDataType"/>
 *         &lt;element name="partyResults" type="{}partyResultsType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "regionResultsType", propOrder = {
    "region",
    "regionData",
    "partyResults"
})
public class RegionResultsType {

    @XmlElement(required = true)
    protected String region;
    @XmlElement(required = true)
    protected PollDataType regionData;
    @XmlElement(required = true)
    protected List<PartyResultsType> partyResults;

    /**
     * Obtiene el valor de la propiedad region.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegion() {
        return region;
    }

    /**
     * Define el valor de la propiedad region.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegion(String value) {
        this.region = value;
    }

    /**
     * Obtiene el valor de la propiedad regionData.
     * 
     * @return
     *     possible object is
     *     {@link PollDataType }
     *     
     */
    public PollDataType getRegionData() {
        return regionData;
    }

    /**
     * Define el valor de la propiedad regionData.
     * 
     * @param value
     *     allowed object is
     *     {@link PollDataType }
     *     
     */
    public void setRegionData(PollDataType value) {
        this.regionData = value;
    }

    /**
     * Gets the value of the partyResults property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the partyResults property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPartyResults().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PartyResultsType }
     * 
     * 
     */
    public List<PartyResultsType> getPartyResults() {
        if (partyResults == null) {
            partyResults = new ArrayList<PartyResultsType>();
        }
        return this.partyResults;
    }

}
