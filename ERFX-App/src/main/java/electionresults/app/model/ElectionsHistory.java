package electionresults.app.model;

import electionresults.model.*;
import electionresults.persistence.io.DataAccessLayer;

import java.util.*;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 29/04/2017.
 */
public class ElectionsHistory {
//    private static final Logger LOGGER = Logger.getLogger(ElectionsHistory.class.getName());
    private final String elections;
    private final Map<String, ProvinceInfo> provinces;
    private final Map<Integer, ElectionResults> historicResults = new TreeMap<>(Comparator.naturalOrder());
    private final List<Integer> electionYears;

    public ElectionsHistory() {
        List<ElectionResults> allElectionResults = DataAccessLayer.getAllElectionResults();
        electionYears = DataAccessLayer.getElectionYears();
        for (ElectionResults electionResults : allElectionResults)
            historicResults.put(electionResults.getYear(), electionResults);
        this.elections = getLastElectionResults().getGlobalResults().getRegion();
        this.provinces = getLastElectionResults().getProvinces();
    }

    public String getElections() {
        return elections;
    }

    public ElectionResults getElectionResults(int year) {
        return historicResults.get(year);
    }

    public ElectionResults getLastElectionResults() {
        int lastElectionYear = electionYears.get(electionYears.size() - 1);
        return historicResults.get(lastElectionYear);
    }


    /******************************************/

    public Map<Integer, RegionResults> getHistoricProvinceResults(String province) {
        Map<Integer, RegionResults> results = new TreeMap<>(Comparator.naturalOrder());
        for (int year : electionYears) {
            results.put(year, historicResults.get(year).getProvinceResults(province));
        }
        return results;
    }

    public Map<Integer, RegionResults> getHistoricGlobalResults() {
        Map<Integer, RegionResults> results = new TreeMap<>(Comparator.naturalOrder());
        for (int year : electionYears) {
            results.put(year, historicResults.get(year).getGlobalResults());
        }
        return results;
    }

    public Map<String, Map<Integer, PartyResults>> getHistoricPartiesResults(List<Party> parties) {
        Map<String, Map<Integer, PartyResults>> resultsByParty = new HashMap<>();
        for (Party party : parties) {
            resultsByParty.put(party.getName(), getHistoricPartyResults(party.getAcronyms()));
        }
        return resultsByParty;
    }

    public Map<Integer, PartyResults> getHistoricPartyResults(List<String> partyAcronyms) {
        Map<Integer, PartyResults> results = new TreeMap<>(Comparator.naturalOrder());
        for (int year : electionYears) {
            PartyResults partyResults = null;
            for (String acronym : partyAcronyms) {
                partyResults = historicResults.get(year).getGlobalResults().getPartyResults(acronym);
                if (partyResults != null) break;
            }
            results.put(year, partyResults);
        }
        return results;
    }
}
