package electionresults.app.model;

import electionresults.model.PartyResults;
import electionresults.model.PollData;
import electionresults.model.RegionResults;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;

import java.util.Map;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 02/05/2017.
 */
public class ChartDataGenerator {

    public static XYChart.Series<String, Number> getRegionParticipationSeries(String region, Map<Integer, RegionResults> historicRegionResults) {
        XYChart.Series<String, Number> series = new XYChart.Series();
        series.setName(region);
        ObservableList<XYChart.Data<String, Number>> data = FXCollections.observableArrayList();
        for (Map.Entry<Integer, RegionResults> entry : historicRegionResults.entrySet()) {
            int year = entry.getKey();
            RegionResults regionResults = entry.getValue();
            PollData pollData = regionResults.getPollData();
            data.add(new XYChart.Data(String.valueOf(year), (double) pollData.getVotes() * 100.0 / pollData.getCensus()));
        }
        series.setData(data);
        return series;
    }

    public static XYChart.Series<String, Number> getRegionParticipationSeries(String region, RegionResults regionResults) {
        XYChart.Series<String, Number> series = new XYChart.Series();
        series.setName(region);
        ObservableList<XYChart.Data<String, Number>> data = FXCollections.observableArrayList();
        PollData pollData = regionResults.getPollData();
        data.add(new XYChart.Data("", (double) pollData.getVotes() * 100.0 / pollData.getCensus()));
        series.setData(data);
        return series;
    }

    public static ObservableList<PieChart.Data> getSeatsDistributionData(RegionResults regionResults) {
        ObservableList<PieChart.Data> data = FXCollections.observableArrayList();
        for (PartyResults partyResults : regionResults.getPartyResultsSorted()) {
            int seats = partyResults.getSeats();
            if (seats > 0)
                data.add(new PieChart.Data(partyResults.getParty(), seats));
        }
        return data;
    }

    public static ObservableList<XYChart.Series<String, Number>> getPartyVotesSeries(int year, RegionResults regionResults, double threshold) {
        ObservableList<XYChart.Series<String, Number>> series = FXCollections.observableArrayList();
        for (PartyResults partyResults : regionResults.getPartyResultsSorted()) {
            if (partyResults.getPercentage() < threshold)  break;
            series.add(getPartyVotesSeries(year, partyResults));
        }
        return series;
    }

    private static XYChart.Series<String, Number> getPartyVotesSeries(int year, PartyResults results) {
        XYChart.Series series = new XYChart.Series();
        series.setName(results.getParty());
        ObservableList<XYChart.Data> partyData = FXCollections.observableArrayList();
        partyData.add(new XYChart.Data(String.valueOf(year), results.getVotes()));
        series.setData(partyData);
        return series;
    }


    public static ObservableList<XYChart.Series<String,Number>> getPartySeatsSeries(Map<String, Map<Integer, PartyResults>> historicPartiesResults) {
        ObservableList<XYChart.Series<String, Number>> series = FXCollections.observableArrayList();
        for (Map.Entry<String, Map<Integer, PartyResults>> entry : historicPartiesResults.entrySet()) {
            series.add(getPartySeatsSeries(entry.getKey(), entry.getValue()));
        }
        return series;
    }

    private static XYChart.Series<String,Number> getPartySeatsSeries(String party, Map<Integer, PartyResults> historicPartyResults) {
        XYChart.Series partySeries = new XYChart.Series();
        partySeries.setName(party);
        ObservableList<XYChart.Data> partyData = FXCollections.observableArrayList();
        for (Map.Entry<Integer, PartyResults> entry : historicPartyResults.entrySet()) {
            if (entry.getValue() == null) continue;
            partyData.add(new XYChart.Data(String.valueOf(entry.getKey()), entry.getValue().getSeats()));
        }
        partySeries.setData(partyData);
        return partySeries;
    }

    public static ObservableList<XYChart.Series<String,Number>> getPartyVotesSeries(Map<String, Map<Integer, PartyResults>> historicPartiesResults) {
        ObservableList<XYChart.Series<String, Number>> series = FXCollections.observableArrayList();
        for (Map.Entry<String, Map<Integer, PartyResults>> entry : historicPartiesResults.entrySet()) {
            series.add(getPartyVotesSeries(entry.getKey(), entry.getValue()));
        }
        return series;
    }

    private static XYChart.Series<String,Number> getPartyVotesSeries(String party, Map<Integer, PartyResults> historicPartyResults) {
        XYChart.Series partySeries = new XYChart.Series();
        partySeries.setName(party);
        ObservableList<XYChart.Data> partyData = FXCollections.observableArrayList();
        for (Map.Entry<Integer, PartyResults> entry : historicPartyResults.entrySet()) {
            if (entry.getValue() == null) continue;
            partyData.add(new XYChart.Data(String.valueOf(entry.getKey()), entry.getValue().getVotes()));
        }
        partySeries.setData(partyData);
        return partySeries;
    }


    public static ObservableList<PieChart.Data> getPollChartData(PollData pollData) {
        ObservableList<PieChart.Data> data = FXCollections.observableArrayList();
        double census = pollData.getCensus();
        data.addAll(
                new PieChart.Data("Abstentions", pollData.getAbstentions() / census * 100.0),
                new PieChart.Data("Null votes", pollData.getNullVotes() / census * 100.0),
                new PieChart.Data("Blank votes", pollData.getBlankVotes() / census * 100.0),
                new PieChart.Data("Party votes", pollData.getCandidateVotes() / census * 100.0)
        );
        return data;
    }


    /**************************/

//
//    public static XYChart.Series<String, Number> getSeatsDistributionSerie(String party, Map<String, RegionResults> provinceResults) {
//        XYChart.Series<String, Number> serie = new XYChart.Series();
//        serie.setName(party);
//        ObservableList<XYChart.Data<String, Number>> datum = FXCollections.observableArrayList();
//        for (Map.Entry<String, RegionResults> entry : provinceResults.entrySet()) {
//            int seats = entry.getValue().getPartyResults(party).getSeats();
//            if (seats > 0) datum.add(new XYChart.Data(entry.getKey(), seats));
//        }
//        serie.setData(datum);
//        return serie;
//    }
//
//    public static XYChart.Series<String, Number> getSeatsDistributionSerie(String region, RegionResults regionResults) {
//        XYChart.Series<String, Number> serie = new XYChart.Series();
//        serie.setName(region);
//        ObservableList<XYChart.Data<String, Number>> datum = FXCollections.observableArrayList();
//        for (PartyResults partyResults : regionResults.getPartyResultsSorted()) {
//            int seats = partyResults.getSeats();
//            if (seats == 0) break;
//            datum.add(new XYChart.Data(partyResults.getParty(), seats));
//        }
//        serie.setData(datum);
//        return serie;
//    }
}
