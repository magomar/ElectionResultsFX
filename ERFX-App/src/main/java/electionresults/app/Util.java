package electionresults.app;

import com.sun.javafx.charts.Legend;
import electionresults.model.Party;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.chart.Chart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;

import java.text.DecimalFormat;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 09/05/2017.
 */
public class Util {
    public static final DecimalFormat PERCENTAGE_FORMAT = new DecimalFormat("#.##");
    public static double computePercentage(double value, double total) {
        return value / total * 100.0;
    }

    public static String getPercentageAsString(double value, double total) {
        return PERCENTAGE_FORMAT.format(computePercentage(value, total)) + "%";
    }

    public static void colorizeLegend(Chart chart){
        Legend legend = (Legend) chart.lookup(".chart-legend");
        for(Legend.LegendItem item:legend.getItems()){
            try{
                item.getSymbol().setStyle("-fx-background-color: " + Party.getPartyByName(item.getText()).getHexColor());
            } catch(NullPointerException e){}
        }
    }

    public static Node findNode(final Parent aParent, final String aClassname, final String aStyleName) {
        if (null != aParent) {
            final ObservableList<Node> children = aParent.getChildrenUnmodifiable();
            if (null != children) {
                for (final Node child : children) {
                    String className = child.getClass().getName();
                    if (className.contains("$")) {
                        className = className.substring(0, className.indexOf("$"));
                    }
                    if (0 == aClassname.compareToIgnoreCase(className)) {
                        if ((null == aStyleName) || (0 == aStyleName.length())) {
                            // No aStyleName, just return this:
                            return child;
                        } else {
                            final String styleName = child.getStyleClass().toString();
                            if (0 == aStyleName.compareToIgnoreCase(styleName)) {
                                return child;
                            }
                        }
                    }
                    if (child instanceof Parent) {
                        final Node node = findNode((Parent) child, aClassname, aStyleName);
                        if (null != node) {
                            return node;
                        }
                    }
                }
            }
        }
        return null;
    }

    public static Path drawSemiRing(double centerX, double centerY, double radius, double innerRadius, Color bgColor, Color strkColor) {
        Path path = new Path();
        path.setFill(bgColor);
        path.setStroke(strkColor);
        path.setFillRule(FillRule.EVEN_ODD);

        MoveTo moveTo = new MoveTo();
        moveTo.setX(centerX + innerRadius);
        moveTo.setY(centerY);

        ArcTo arcToInner = new ArcTo();
        arcToInner.setX(centerX - innerRadius);
        arcToInner.setY(centerY);
        arcToInner.setRadiusX(innerRadius);
        arcToInner.setRadiusY(innerRadius);

        MoveTo moveTo2 = new MoveTo();
        moveTo2.setX(centerX + innerRadius);
        moveTo2.setY(centerY);

        HLineTo hLineToRightLeg = new HLineTo();
        hLineToRightLeg.setX(centerX + radius);

        ArcTo arcTo = new ArcTo();
        arcTo.setX(centerX - radius);
        arcTo.setY(centerY);
        arcTo.setRadiusX(radius);
        arcTo.setRadiusY(radius);

        HLineTo hLineToLeftLeg = new HLineTo();
        hLineToLeftLeg.setX(centerX - innerRadius);

        path.getElements().add(moveTo);
        path.getElements().add(arcToInner);
        path.getElements().add(moveTo2);
        path.getElements().add(hLineToRightLeg);
        path.getElements().add(arcTo);
        path.getElements().add(hLineToLeftLeg);

        return path;
    }
}
