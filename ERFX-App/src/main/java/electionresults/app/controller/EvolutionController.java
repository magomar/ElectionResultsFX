package electionresults.app.controller;

import electionresults.app.Util;
import electionresults.app.model.ChartDataGenerator;
import electionresults.app.model.ElectionsHistory;
import electionresults.model.Party;
import electionresults.model.PartyResults;
import electionresults.model.Province;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.fxml.FXML;
import javafx.scene.chart.*;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 04/05/2017.
 */
public class EvolutionController {

    @FXML
    private BarChart<String, Number> participationChart;

    @FXML
    private FlowPane partyFlowPane;

    @FXML
    private LineChart<String, Number> partyVotesChart;

    @FXML
    private StackedBarChart<String, Number> partySeatsChart;

    private ElectionsHistory electionsHistory;
    private Map<String, ToggleButton> toggleButtonMap = new HashMap<>();

    @FXML
    private void initialize() {
        for (Party party : Party.values()) {
            ToggleButton toggleButton = new ToggleButton();
            ImageView logo = new ImageView(party.getLogo());
            logo.setFitWidth(35);
            logo.setFitHeight(35);
            toggleButton.setGraphic(logo);
            toggleButton.setTooltip(new Tooltip(party.getName()));
            toggleButton.setSelected(true);
            partyFlowPane.getChildren().add(toggleButton);
            toggleButtonMap.put(party.getName(), toggleButton);
        }
        ((NumberAxis) participationChart.getYAxis()).setUpperBound(100);
        ((NumberAxis) partySeatsChart.getYAxis()).setUpperBound(Province.getTotalSeats());
    }

    public void setData(ElectionsHistory electionsHistory) {
        if (this.electionsHistory == null) {
            this.electionsHistory = electionsHistory;
            populateParticipationChart();
            Map<String, Map<Integer, PartyResults>> historicPartyResults = electionsHistory.getHistoricPartiesResults(Arrays.asList(Party.values()));
            populatePartyVotesChart(historicPartyResults);
            populatePartySeatsChart(historicPartyResults);
        }
    }

    private void populatePartyVotesChart(Map<String, Map<Integer, PartyResults>> historicPartyResults) {
        partyVotesChart.setData(ChartDataGenerator.getPartyVotesSeries(historicPartyResults));
        for (XYChart.Series<String, Number> serie : partyVotesChart.getData()) {
            String party = serie.getName();
            addSeriesToggleListener(partyVotesChart, toggleButtonMap.get(party).selectedProperty(), serie);
        }
        Platform.runLater(() -> styleVotesLineChart());
        Platform.runLater(() -> Util.colorizeLegend(partyVotesChart));
    }


    private void styleVotesLineChart() {
        for (XYChart.Series<String, Number> serie : partyVotesChart.getData()) {
            String party = serie.getName();
            StringBuilder serieStyle = new StringBuilder();
            String rgbColor = Party.getPartyByName(party).getHexColor();
            serieStyle.append("-fx-stroke: " + rgbColor + ";");
            serie.getNode().setStyle(serieStyle.toString());
            for (XYChart.Data<String, Number> datum : serie.getData()) {
                StringBuilder datumStyle = new StringBuilder();
                datumStyle.append("-fx-background-color: " + rgbColor + "; ");
                datum.getNode().setStyle(datumStyle.toString());
            }
        }
    }

    private void populatePartySeatsChart(Map<String, Map<Integer, PartyResults>> historicPartyResults) {
        partySeatsChart.setData(ChartDataGenerator.getPartySeatsSeries(historicPartyResults));
        for (XYChart.Series<String, Number> serie : partySeatsChart.getData()) {
            String party = serie.getName();
            addSeriesToggleListener(partySeatsChart, toggleButtonMap.get(party).selectedProperty(), serie);
        }
        Platform.runLater(() -> stylePartySeatsChart());
        Platform.runLater(() -> Util.colorizeLegend(partySeatsChart));
    }

    private void stylePartySeatsChart() {
        for (XYChart.Series<String, Number> serie : partySeatsChart.getData()) {
            String party = serie.getName();
            for (XYChart.Data<String, Number> datum : serie.getData()) {
                StringBuilder style = new StringBuilder();
                String rgbColor = Party.getPartyByName(party).getHexColor();
                style.append("-fx-bar-fill: " + rgbColor + "; ");
                datum.getNode().setStyle(style.toString());
            }
        }
    }

    private void populateParticipationChart() {
        participationChart.getData().add(ChartDataGenerator.getRegionParticipationSeries(electionsHistory.getElections(), electionsHistory.getHistoricGlobalResults()));
        for (String province : electionsHistory.getLastElectionResults().getProvinces().keySet()) {
            participationChart.getData().add(ChartDataGenerator.getRegionParticipationSeries(province, electionsHistory.getHistoricProvinceResults(province)));
        }
        participationChart.getData().forEach(series -> {
            for (XYChart.Data<String, Number> data : series.getData()) {
                StackPane bar = (StackPane) data.getNode();
                final Text dataText = new Text(String.format("%.1f", data.getYValue()) + "");
                bar.getChildren().add(dataText);
            }
        });
    }

    private void addSeriesToggleListener(XYChart chart, BooleanProperty selected, final XYChart.Series serie) {
        selected.addListener((observable, wasSelected, isSelected) -> {
            if (isSelected) {
                chart.getData().add(serie);
            }
            if (!isSelected){
                chart.getData().remove(serie);
            }
            Platform.runLater(() -> stylePartySeatsChart());
            Platform.runLater(() -> styleVotesLineChart());
            Platform.runLater(() -> Util.colorizeLegend(partySeatsChart));
            Platform.runLater(() -> Util.colorizeLegend(partyVotesChart));
        });
    }


}
