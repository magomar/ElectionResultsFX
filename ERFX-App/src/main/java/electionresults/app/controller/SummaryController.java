package electionresults.app.controller;

import electionresults.app.Util;
import electionresults.app.model.ChartDataGenerator;
import electionresults.app.model.ElectionsHistory;
import electionresults.model.*;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import static electionresults.model.Province.*;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 28/04/2017.
 */


public class SummaryController {

    @FXML
    private Text mapTitle;

    @FXML
    private Text mapSubtitle;

    @FXML
    private ChoiceBox<String> regionChoice;

    @FXML
    private ImageView mapImageView;

    @FXML
    private ToggleGroup electionYear;

    @FXML
    private TableView<PartyResults> partyResultsTable;

    @FXML
    private TableColumn<PartyResults, String> party;

    @FXML
    private TableColumn<PartyResults, Number> partySeats;

    @FXML
    private TableColumn<PartyResults, Number> partyVotes;

    @FXML
    private TableColumn<PartyResults, Double> partyPercent;

    @FXML
    private PieChart seatsDistributionChart;

    @FXML
    private BarChart<String, Number> partyVotesChart;

    @FXML
    private PollDataController pollDataController;

    @FXML
    private Slider partyFilterSlider;

    @FXML
    private Label partyFilterValue;

    private final ObservableList<PartyResults> parties = FXCollections.observableArrayList();
    private ElectionResults electionResults;
    private ElectionsHistory electionsHistory;
    private StringProperty selectedConstituency = new SimpleStringProperty();
    private IntegerProperty selectedYear = new SimpleIntegerProperty();
    private Image tricolorMap = new Image(ClassLoader.getSystemResourceAsStream("images/maps/map_cv_selector.png"));
    private Map<String, Image> provinceMaps = new HashMap<>();
    private Map<String, ObservableList<String>> regionsByProvince = new HashMap<>();
    private RegionResults regionResults;

    @FXML
    private void initialize() {
        partyResultsTable.setItems(parties);
        partyResultsTable.sort();
        party.setCellValueFactory(new PropertyValueFactory("party"));
        partyVotes.setCellValueFactory(new PropertyValueFactory<>("votes"));
        partySeats.setCellValueFactory(new PropertyValueFactory<>("seats"));
        partyPercent.setCellValueFactory(new PropertyValueFactory<>("percentage"));
        partyPercent.setCellFactory(c -> new PartyPercentageTableCell());

        selectedYear.addListener((observable, oldValue, newValue) -> {
            int year = newValue.intValue();
            electionResults = electionsHistory.getElectionResults(year);
            showConstituencyResults(selectedConstituency.get());
            showRegionResults(regionChoice.getSelectionModel().getSelectedItem());
            regionsByProvince.clear();
            regionsByProvince.put(electionsHistory.getElections(), FXCollections.observableArrayList(electionsHistory.getLastElectionResults().getRegionProvinces().keySet()));
            for (Province province : Province.values()) {
                String provinceName = province.getFullname();
                regionsByProvince.put(provinceName, FXCollections.observableArrayList(electionsHistory.getElectionResults(year).getProvinces().get(provinceName).getRegions()));
            }
        });
        regionChoice.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> showRegionResults(newValue));
        selectedConstituency.addListener((observable, oldValue, newValue) -> showConstituencyResults(newValue));
        partyFilterSlider.valueProperty().addListener(observable -> {
            partyResultsTable.refresh();
            populatePartyVotesChart();
        });
        partyFilterValue.textProperty().bind(partyFilterSlider.valueProperty().asString("%.2f"));
    }

    @FXML
    void selectProvince(MouseEvent event) {
        int x = (int) event.getX() * 2;
        int y = (int) event.getY() * 2;
        Color color = tricolorMap.getPixelReader().getColor(x, y);
//        System.out.println("Mouse clicked on (" + x +", " + y + "). Color = " + color);
        if (color.equals(Color.rgb(0, 0, 255))) selectedConstituency.set(VALENCIA.getFullname());
        else if (color.equals(Color.rgb(0, 255, 0))) selectedConstituency.set(ALICANTE.getFullname());
        else if (color.equals(Color.rgb(255, 0, 0))) selectedConstituency.set(CASTELLON.getFullname());
        else selectedConstituency.set(electionsHistory.getElections());

    }

    @FXML
    void selectYear(ActionEvent event) {
        ToggleButton toggleButton = (ToggleButton) event.getSource();
        String yearString = toggleButton.getText();
        try {
            Integer year = Integer.valueOf(yearString);
            selectedYear.set(year);

        } catch (NumberFormatException ex) {
            // TODO All years selected
        }
    }

    public void setData(ElectionsHistory electionsHistory) {
        if (this.electionsHistory == null) {
            this.electionsHistory = electionsHistory;
            String elections = electionsHistory.getElections();
            provinceMaps.put(elections, mapImageView.getImage());
            for (Province province : Province.values()) {
                InputStream inputStream = getClass().getResourceAsStream(province.getImagePath());
                provinceMaps.put(province.getFullname(), new Image(inputStream));

            }
            selectedYear.set(2015);
            selectedConstituency.set(elections);
        }
    }

    private void showConstituencyResults(String constituency) {
        if (constituency == null) return;
        RegionResults regionResults;
        if (constituency.equals(electionsHistory.getElections())) {
            regionResults = electionResults.getGlobalResults();
            pollDataController.setData(Province.getTotalSeats());
        } else {
            regionResults = electionResults.getProvinceResults(constituency);
            pollDataController.setData(Province.getSeatsByProvince(constituency));
        }
        showCommonResults(regionResults);
        populateSeatsDistributionChart(constituency);
        mapImageView.setImage(provinceMaps.get(constituency));
        mapTitle.setText(constituency);
        mapSubtitle.setVisible(false);
        regionChoice.setItems(regionsByProvince.get(constituency));
    }

    private void showRegionResults(String region) {
        if (region == null) return;
        mapSubtitle.setText(region);
        mapSubtitle.setVisible(true);
        showCommonResults(electionResults.getRegionResults().get(region));
        pollDataController.setData(0);
    }

    private void showCommonResults(RegionResults regionResults) {
        this.regionResults = regionResults;
        parties.setAll(regionResults.getPartyResultsSorted());
        pollDataController.setData(regionResults);
        populatePartyVotesChart();
    }

    private void populateSeatsDistributionChart(String constituency) {
        seatsDistributionChart.setData(ChartDataGenerator.getSeatsDistributionData(regionResults));
        seatsDistributionChart.setTitle("Seats distribution for " + constituency);
        seatsDistributionChart.getData().forEach(datum -> {
                    Party party = Party.getPartyByName(datum.getName());
                    if (party != null)
                        datum.getNode().setStyle("-fx-pie-color: " + party.getHexColor() + ";");
                    datum.nameProperty().bind(Bindings.concat(datum.getName(), " (", (int) datum.getPieValue(), ")"));
                }
        );
    }

    private void populatePartyVotesChart() {
        partyVotesChart.setData(ChartDataGenerator.getPartyVotesSeries(selectedYear.get(), regionResults, partyFilterSlider.getValue()));
        partyVotesChart.setTitle("Party votes in " + regionResults.getRegion());
        Platform.runLater(() -> styleVotesBarChart());
        Platform.runLater(() -> Util.colorizeLegend(partyVotesChart));
    }

    private void styleVotesBarChart() {
        for (XYChart.Series<String, Number> serie : partyVotesChart.getData()) {
            for (XYChart.Data<String, Number> datum : serie.getData()) {
                StringBuilder style = new StringBuilder();
                Party party = Party.getPartyByName(serie.getName());
                if (party != null) {
                    String rgbColor = party.getHexColor();
                    style.append("-fx-bar-fill: " + rgbColor + "; ");
                    datum.getNode().setStyle(style.toString());
                }

                StackPane bar = (StackPane) datum.getNode();
                String votes = String.valueOf(datum.getYValue());
                Text barText = new Text(votes);
                AnchorPane anchorPane = new AnchorPane(barText);
                AnchorPane.setTopAnchor(barText, -15.0);
                AnchorPane.setLeftAnchor(barText, 0.0);
                bar.getChildren().add(anchorPane);
            }
        }
    }

    private class PartyPercentageTableCell extends TableCell<PartyResults, Double> {

        @Override
        protected void updateItem(Double item, boolean empty) {
            super.updateItem(item, empty);
            TableRow<PartyResults> currentRow = getTableRow();
            PartyResults session = currentRow.getItem();
            if (session == null || empty) {
                setText(null);
                setGraphic(null);
                setStyle(null);
                getStyleClass().removeAll(".green-cell", ".yellow-cell");
            } else {
                setText(Util.PERCENTAGE_FORMAT.format(item));
                if (item.doubleValue() >= 5) {
                    getStyleClass().add(".green-cell");
                    setBackground(new Background(new BackgroundFill(Paint.valueOf(Color.LIGHTGREEN.toString()), null, null)));
                } else if (item.doubleValue() >= partyFilterSlider.getValue()) {
                    getStyleClass().add(".yellow-cell");
                    setBackground(new Background(new BackgroundFill(Paint.valueOf(Color.YELLOW.toString()), null, null)));
                }

            }

        }
    }


}