package electionresults.app.controller;/*
/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 27/04/2017.
 */


import electionresults.app.model.ElectionsHistory;
import electionresults.model.Province;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.StackPane;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public class MainController {
    private static final Logger LOG = Logger.getLogger(MainController.class.getName());

    @FXML
    private StackPane stackPane;

    @FXML
    private ToggleGroup viewSelector;

    private final Map<View, Parent> moduleViews = new HashMap<>(View.values().length);
    private final Map<View, ?> moduleControllers = new HashMap<>(View.values().length);
    private View currentView;
    private ElectionsHistory electionsHistory;

    private enum View {
//        SPLASH("SplashScreen.fxml"),
        SUMMARY("Summary.fxml"),
        EVOLUTION("Evolution.fxml");
        private final String fxmlResourcePath;

        View(String fxmlFilename) {
            fxmlResourcePath = "/fxml/" + fxmlFilename;
        }
    }

    @FXML
    private void initialize() {
        electionsHistory = new ElectionsHistory();
        String fileEncodingTestString = electionsHistory.getLastElectionResults().getProvinces().keySet().toString();
        String codeEncodingTestString = Arrays.asList(Province.values()).toString();
        if (!fileEncodingTestString.equals(codeEncodingTestString)) {
            LOG.severe("Warning ! There is a discrepancy between the encoding used in files and the encoding used in code");
            LOG.severe("This problem will produce severe errors in the application");
        }
        for (View view : View.values()) {
            loadModule(view);
        }
        loadSummaryView();
//        loadEvolutionView();
    }

//    @FXML
//    private void loadSplashScreenView() {
//        loadView(View.SPLASH);
//        SplashScreenController controller = (SplashScreenController) moduleControllers.get(View.SPLASH);
//        controller.resume();
//    }

    @FXML
    private void loadSummaryView() {
        loadView(View.SUMMARY);
        SummaryController controller = (SummaryController) moduleControllers.get(View.SUMMARY);
        controller.setData(electionsHistory);
    }

    @FXML
    void loadEvolutionView() {
        loadView(View.EVOLUTION);
        EvolutionController controller = (EvolutionController) moduleControllers.get(View.EVOLUTION);
        controller.setData(electionsHistory);
    }


    private void loadModule(View view) {
        if (moduleViews.containsKey(view)) return;
        else {
            FXMLLoader fxmlLoader = new FXMLLoader(MainController.class.getResource(view.fxmlResourcePath));
            Parent moduleContentPane = null;
            try {
                moduleContentPane = fxmlLoader.load();
                moduleContentPane.setOpacity(0);
                moduleViews.put(view, moduleContentPane);
                moduleControllers.put(view, fxmlLoader.getController());
                stackPane.getChildren().add(moduleContentPane);
            } catch (IOException e) {
                e.printStackTrace();
                LOG.severe("Error loading module " + view.name());
                LOG.severe(e.getMessage());
            }


        }
    }

    private void loadView(View view) {
//        SplashScreenController controller = (SplashScreenController) moduleControllers.get(View.SPLASH);
//        controller.pause();
        if (currentView != null) moduleViews.get(currentView).setOpacity(0);
        currentView = view;
        Parent node = moduleViews.get(view);
        stackPane.getChildren().remove(node);
        stackPane.getChildren().add(node);
        node.setOpacity(1);
    }
}