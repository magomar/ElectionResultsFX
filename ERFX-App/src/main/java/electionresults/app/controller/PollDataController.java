package electionresults.app.controller;

import electionresults.app.Util;
import electionresults.model.PollData;
import electionresults.model.RegionResults;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 03/05/2017.
 */
public class PollDataController {
    @FXML
    private GridPane contentPane;

    @FXML
    private Label summaryTitle;

    @FXML
    private TextField seats;

    @FXML
    private TextField votes;

    @FXML
    private TextField abstentions;

    @FXML
    private TextField nullVotes;

    @FXML
    private TextField blankVotes;

    @FXML
    private TextField votesPerc;

    @FXML
    private TextField abstentionsPerc;

    @FXML
    private TextField nullVotesPerc;

    @FXML
    private TextField blankVotesPerc;

    @FXML
    private void initialize() {
    }

    public void setData(RegionResults regionResults) {
        PollData pollData = regionResults.getPollData();
        int votes = pollData.getVotes();
        int abstentions = pollData.getAbstentions();
        int nullVotes = pollData.getNullVotes();
        int blankVotes = pollData.getBlankVotes();
        int census = pollData.getCensus();
        this.votes.setText(String.valueOf(votes));
        votesPerc.setText(Util.getPercentageAsString(votes, census));
        this.abstentions.setText(String.valueOf(abstentions));
        abstentionsPerc.setText(Util.getPercentageAsString(abstentions, census));
        this.nullVotes.setText(String.valueOf(nullVotes));
        nullVotesPerc.setText(Util.getPercentageAsString(nullVotes, votes));
        this.blankVotes.setText(String.valueOf(blankVotes));
        blankVotesPerc.setText(Util.getPercentageAsString(blankVotes, votes));
    }

    public void setData(int seats) {
        this.seats.setText(String.valueOf(seats));
    }
}
